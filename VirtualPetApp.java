//Import the java Scanner
import java.util.Scanner;

public class VirtualPetApp {
	
	public static void main (String[] args) {
		
		//Create array object from Tiger.java
		Tiger[] ambushOfTigers = new Tiger[4];
		
		//Make a loop asking the user to input the 3 fields for each of the 4 tigers.
		for (int i = 0; i < ambushOfTigers.length; i++) {
			
			//Create a scanner
			Scanner reader = new Scanner(System.in);
			
			//Create tiger object at each index of the array
			ambushOfTigers[i] = new Tiger();
			
			System.out.println("Pick a color for a tiger");
			ambushOfTigers[i].color = reader.next();
			
			System.out.println("Pick a breed for a tiger");
			ambushOfTigers[i].breed = reader.next();
			
			System.out.println("Choose a run speed, in km/h, for a tiger");
			ambushOfTigers[i].runSpeed = reader.nextDouble();
		}
		
		//Describe the 3 fields of the last tiger.
		System.out.println("The last tiger's fur color is " + ambushOfTigers[ambushOfTigers.length - 1].color + ".");
		System.out.println("The last tiger's breed is " + ambushOfTigers[ambushOfTigers.length - 1].breed + ".");
		System.out.println("The last tiger's run speed is " + ambushOfTigers[ambushOfTigers.length - 1].runSpeed + " km/h.");
				
		
		//Describe the actions the first tiger is doing. This action is run() method in Tiger.java
		System.out.print("The first tiger is");
		ambushOfTigers[0].run();
		
		//Skip a line to make it look cleaner and less messy
		System.out.println();
		
		//Describe the actions the first tiger is doing. This action is drink() method in Tiger.java
		System.out.print("The first tiger is"); 
		ambushOfTigers[0].drink();
		
	}
}
	