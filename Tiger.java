public class Tiger {

	//Create the 3 field variables
	public String color;
	public String breed;
	public double runSpeed;
	
	public void run() {		
		//Add specific messages depending on the tiger's speed;
		if (runSpeed < 0) {
			System.out.print(" running at " + runSpeed + " km/h. How can it run at negative speed?!?!?!");
		}
		
		else if (runSpeed <= 30) {
			System.out.print(" running at only " + runSpeed + " km/h. What a slow tiger!");
		}
		
		else if (runSpeed > 30) {
			System.out.print(" running at " + runSpeed + " km/h. Pretty decent speed for a tiger.");
		}
		
		else if (runSpeed > 50) {
			System.out.print(" running at " + runSpeed + " km/h. That's a fast tiger wowww!");
		}
		
		else if (runSpeed > 100) {
			System.out.print(" running at an extreme speed of " + runSpeed + " km/h. How is that even possible?!?!?!?!?");
		}
	}
	
	public void drink() {	
		System.out.print(" currently drinking water. He needs to stay hydrated after running that much!");
	}
}